# showImages

Para instalar correctamente este proyecto primero tenga en cuenta que debe tener instaladas las siguientes liberías en su máquina:

NodeJs: https://nodejs.org/en/
Sass: http://sass-lang.com/install

Luego de haber instala correctamente las mencionadas librerías podemos descargar el proyecto:

git clone https://jhon009@bitbucket.org/jhon009/showimages.git

Después de descargar el proyecto, con el paquete npm que node nos proporciona podemos descargar los archivos necesario para hacer andar el proyecto, 
para ello en nuestra terminal de comandos nos vamos a posicionar en la raíz de proyecto y ejecutar:

npm install

Luego de que se hayan descargados todos los paquetes podremos digitar en nuesra terminal de comandos el siguiente comando:

npm run start

Con esto nuestro proyecto ya debe estar visible en http://localhost:9000/
Si por alguna razón tenemos el puerto 9000 ya ocupado, podemos cambiar este en nuestro archivo webpack.config.js