import React, { Component } from 'react';
import { connect } from 'react-redux';
import PhotoItem from '../../components/PhotoItem';

import './styles.scss';

const Modal = ({ match, history, photos }) => {
  const item = photos.items.get(match.params.page)[match.params.id];

  function back(e) {
    e.stopPropagation()
    history.goBack()
  }
  return (
    <div className="Modal modal d-block" role="dialog">
      <div className="Modal-dialog modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title text-center">{item.title}</h5>
            <button type="button" onClick={back} className="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <PhotoItem item={item} itemkey={0} modal={true} />
          </div>
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = state => {
  return {
    photos: state
  }
}

export default connect(mapStateToProps)(Modal);