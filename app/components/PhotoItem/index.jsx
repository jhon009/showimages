// @flow
import React, { Component } from 'react';
import { PhotosTypes } from '../../data/photos';

import './styles.scss';

type ShowImagesContainerProps = {
  itemkey: number,
  item: PhotosTypes,
  modal?: boolean
}

export default function PhotoItem(props:ShowImagesContainerProps):React.ComponentType<any> {
  return (
    <div className="PhotoItem-card card" key={props.itemkey}>
      <img className="card-img-top" src={(props.modal) ? props.item.url : props.item.thumbnailUrl} alt="Card image cap" />
      {!props.modal && <div className="card-body">
        <p className="card-text">{props.item.title}</p>
      </div>}
    </div>
  );
}