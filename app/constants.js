const CONSTANTS = {
    PHOTOS_API: 'https://jsonplaceholder.typicode.com/photos'
};

export default CONSTANTS;