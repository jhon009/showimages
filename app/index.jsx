import 'babel-polyfill';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import './styles.scss';
import RouterContainer from './Routes/Router';
import configStore from './data/store';

const store = configStore();

ReactDOM.render(
  <Provider store={store}>
    <RouterContainer />
  </Provider>,
  document.getElementById('app')
);