// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { actions } from '../../data/actions';
import { PhotosTypes } from '../../data/photos';
import { fromJS } from 'immutable';
import PhotoItem from '../../components/PhotoItem';
import { Link } from 'react-router-dom';

import './styles.scss';

type ShowImagesContainerProps = {
  fetchPhotos: Function,
  photos: PhotosTypes
}

class ShowImagesContainer extends Component {

  state = {
    page: 0,
    photos: []
  }
  
  componentWillMount() {
    this.props.fetchPhotos();
  }

  componentWillReceiveProps(props) {
    if (this.state.page !== 0) return false;
    this.setState({
      photos: (props.photos.items.size)? props.photos.items.get(0) : []
    })
  }

  renderLists(item, key) {
    return (
      <div key={key} className="col-12 col-sm-4 mb-5">
        <Link
          key={key}
          to={{
            pathname: `/page/${this.state.page}/img/${key}`,
            state: { modal: true }
          }}
        >
          <PhotoItem item={item} itemkey={key} key={key} />
        </Link>
      </div>
    );
  }

  changePage(page) {
    this.setState({
      page: page,
      photos: this.props.photos.items.get(page)
    });
  }

  renderPaginator(item, key) {
    return (
      <li className={`page-item ${(this.state.page === (key))? 'active':''}`} key={key}>
        <span className="page-link" onClick={this.changePage.bind(this, key)}>{key + 1}</span>
      </li>
    );
  }

  showLoader() {
    return (
      <div className="loader">
        <div className="loader-text text-center">LOADING...</div>
      </div>
    );
  }

  alertMessage(message, type) {
    return (
      <div className={`alert alert-${type} w-100 text-center`} role="alert">
        {message}
      </div>
    );
  }
  
  render():React.ComponentType<any> {
    const list = this.state.photos.map(this.renderLists.bind(this));
    const paginator = this.props.photos.items.map(this.renderPaginator.bind(this));
    const loader = this.showLoader.bind(this);
    const alertMessage = this.alertMessage.bind(this);
    return (<div className="ShowImagesContainer row">
      { this.props.photos.isPending && loader()}
      <div className="row d-block w-100 text-center p-5">
        <h1>SHOW IMAGES LYCA<span>ON</span></h1>
      </div>
      {(this.props.photos.fetchFailed) ? alertMessage(this.props.photos.failedMessage, 'danger') : list}
      { (!this.props.photos.fetchFailed && !this.props.photos.isPending) && <div className="row paginator m-0 p-3">
        <li className={`page-item ${(this.state.page === 0)? 'disabled':''}`}>
          <span className="page-link" onClick={this.changePage.bind(this, (this.state.page - 1))}>Previous</span>
        </li>
        {paginator}
        <li className={`page-item ${(this.state.page === (this.props.photos.items.size - 1))? 'disabled':''}`}>
          <span className="page-link" onClick={this.changePage.bind(this, (this.state.page + 1))}>Next</span>
        </li>
      </div>}
    </div>);
  }
}

const mapStateToProps = state => {
  return {
    photos: state
  }
}

const mapDispatchToProps = {
  fetchPhotos: actions.fetchPhotos
};

export default connect(mapStateToProps, mapDispatchToProps)(ShowImagesContainer)
