import { put, takeLatest, takeEvery, all, fork, call } from 'redux-saga/effects';
import { actions } from './actions';
import api from '../api';

export function* initApp() {
  try {
    const photosInfo = yield call(api.fetchPhotos);
    yield put({
      type: actions.PHOTOS_REQUEST_SUCCEEDED,
      photos: photosInfo
    });
  } catch (e) {
    yield put({
      type: actions.PHOTOS_REQUEST_FAILED,
      message: 'No se han podido cargar las imágenes correctamente.'
    })
  }
}

export function* watchInitApp() {
  yield takeEvery(actions.INIT_APP, initApp);
}

export const appSagas = [
  fork(watchInitApp)
];

export default function* sagas() {
  yield all([
    ...appSagas
  ])
}