import { Photos, createPhotosList } from './photos';
import { actions } from './actions';
import { List } from 'immutable';

const reducer = (state = new Photos(), action) => {
  switch (action.type) {
    case actions.INIT_APP:
      return state.set('isPending', true);
    case actions.PHOTOS_REQUEST_FAILED:
      return state.withMutations(photos => {
        photos.set('fetchFailed', true);
        photos.set('isPending', false);
        photos.set('failedMessage', action.message);
      });
    case actions.PHOTOS_REQUEST_SUCCEEDED:
      return state.withMutations(photos => {
        photos.set('isPending', false);
        photos.set('items', new List(createPhotosList(action.photos)));
      }); 
    default:
      return state
  }
  return state;
}

export default reducer;