export const actions = {
  INIT_APP: 'INIT_APP',
  PHOTOS_REQUEST_SUCCEEDED: 'PHOTOS_REQUEST_SUCCEEDED',
  PHOTOS_REQUEST_FAILED: 'PHOTOS_REQUEST_FAILED',
  fetchPhotos: () => ({
    type: actions.INIT_APP
  })
};