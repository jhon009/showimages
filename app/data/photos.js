import { List, Record} from 'immutable';

export type PhotosTypes = {
  isPending: boolean,
  fetchFailed: boolean,
  failedMessage: string,
  items: Array<PhotosItemType>
}

export type PhotosItemType = {
  albumId: number,
  id: number,
  title: string,
  url: string,
  thumbnailUrl: string
};

export const Photos = Record({
  isPending: false,
  items: new List(),
  fetchFailed: false,
  failedMessage: ''
});

function splitArray(myArray, arraySize){
  const arrayLength = myArray.length;
  let arraySlice = {};
  let result = [];
  
  for (let i = 0; i < arrayLength; i += arraySize) {
      arraySlice = myArray.slice(i, (i + arraySize));
      result.push(arraySlice);
  }

  return result;
}

export const createPhotosList = (data) => {
  const lists = splitArray(data, 50);
  return lists;
};
