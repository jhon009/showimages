import request from 'superagent';
import CONSTANTS from '../constants';

const api = {
  fetchPhotos() {
    return dispatch({
      method: 'get',
      url: CONSTANTS.PHOTOS_API
    })
  }
};

function dispatch(options) {
  return dispatchNoRetry(options).catch(response => {
    throw response;
  })
}

function dispatchNoRetry(options) {
  return request[options.method](options.url)
  .then(response => response.body);
}

export default api;
