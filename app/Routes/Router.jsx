import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import RoutesContainer from './Routes';


export default function RouterContainer() {
  return (
    <BrowserRouter>
      <Route component={RoutesContainer} />
    </BrowserRouter>
  );
}