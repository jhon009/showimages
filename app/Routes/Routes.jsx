import React, { Component } from 'react';
import ShowImagesContainer from '../ShowImages/container';
import Modal from '../Modal/container';
import {
  Switch,
  Route
} from 'react-router-dom';

export default class RoutesContainer extends React.Component {
  previousLocation = this.props.location

  componentWillUpdate(nextProps) {
    const { location } = this.props

    if (
      nextProps.history.action !== 'POP' &&
      (!location.state || !location.state.modal)
    ) {
      this.previousLocation = this.props.location
    }
  }

  render() {
    const { location } = this.props
    const isModal = !!(
      location.state &&
      location.state.modal &&
      this.previousLocation !== location
    )
    return (
      <div className="container">
        <Switch location={isModal ? this.previousLocation : location}>
          <Route exact path='/' component={ShowImagesContainer}/>
        </Switch>
        {isModal ? <Route path='/page/:page/img/:id' component={Modal} /> : null}
      </div>
    )
  }
}